<?php 

class SecurityController extends Controller 
{
	public function detailAction(array $routeParams)
	{
		$userModel = new UserModel();
		$securityDetails = $userModel->getSecurityDetailsByUserId($this->session->user->userId);

		$this->renderView(array('securityDetails' => $securityDetails), 'Security/detail.phtml');
	}

	public function editAction(array $routeParams)
	{

		$viewVars = array();
		$userModel = new UserModel();
		$securityDetails = $userModel->getSecurityDetailsByUserId($this->session->user->userId);

		if ($_POST) 
		{
			try
			{
				CsrfUtil::validateForm();

				$securityDetails->status = $this->post('status', new RequestOptionsEntity(array(
						'defaultValue' => UserModel::INACTIVE_USER,
						'dataType' => 'int',
						'maxValue' => '3'
					)));

				$securityDetails->pinTransferLimit = $this->post('pinTransferLimit', new RequestOptionsEntity(array(
						'required' => true,
						'dataType' => 'float_es',
						'maxValue' => '300000'
					)));

				$userModel->updatePinTransferLimit($securityDetails);

				header('Location:/seguridad');
			}
			catch (Exception $e)
			{
				$viewVars['pageError'] = $e->getMessage();
			}
		}

		$viewVars['securityDetails'] = $securityDetails;

		$this->renderView($viewVars, 'Security/edit.phtml');
	}

	public function editApiAction(array $routeParams)
	{
		$userModel = new UserModel();
		$securityDetails = $userModel->getSecurityDetailsByUserId($this->session->user->userId);
			
		try
		{
			CsrfUtil::validateForm(true);

			$securityDetails->status = $this->post('status', new RequestOptionsEntity(array(
					'defaultValue' => UserModel::INACTIVE_USER,
					'dataType' => 'int',
					'maxValue' => '3'
				)));

			$securityDetails->pinTransferLimit = $this->post('pinTransferLimit', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'float_es',
					'maxValue' => '300000'
				)));

			$userModel->updatePinTransferLimit($securityDetails);

			$this->sendSuccess('OK');
		}
		catch (Exception $e)
		{
			$this->sendError($e->getMessage());
		}
	}

	public function pinEditAction()
	{
		$viewVars = array();
		$userModel = new UserModel();

		if ($_POST) 
		{
			try
			{
				CsrfUtil::validateForm();

				// $currentPin = $this->post('currentPin', new RequestOptionsEntity(array(
				// 		'required' => true,
				// 		'dataType' => 'int',
				// 		'maxValue' => '99999'
				// 	)));

				$newPin = $this->post('newPin', new RequestOptionsEntity(array(
						'required' => true,
						'dataType' => 'int',
						'maxValue' => '99999'
					)));

				$repeatPin = $this->post('repeatPin', new RequestOptionsEntity(array(
						'required' => true,
						'dataType' => 'int',
						'maxValue' => '99999'
					)));

				if ($newPin !== $repeatPin)
					throw new \Exception(_('El nuevo PIN enviado no coincide en los valores suministrados. Escriba ambos campos nuevamente e intente de nuevo.'));

				// if ($userModel->checkPin($this->session->user->userId, $currentPin) === false)
				// 	throw new \Exception(_('El PIN actual suministrado no coincide con el PIN almacenado'));

				$userModel->updatePin($this->session->user->userId, $newPin);

				header('Location:/seguridad');
			}
			catch (Exception $e)
			{
				$viewVars['pageError'] = $e->getMessage();
			}
		}

		$this->renderView($viewVars, 'Security/pin.phtml');
	}

	public function pinEditApiAction()
	{
		$userModel = new UserModel();

		try
		{
			CsrfUtil::validateForm(true);

			// $currentPin = $this->post('currentPin', new RequestOptionsEntity(array(
			// 		'required' => true,
			// 		'dataType' => 'int',
			// 		'maxValue' => '99999'
			// 	)));

			$newPin = $this->post('newPin', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'int',
					'maxValue' => '99999'
				)));

			$repeatPin = $this->post('repeatPin', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'int',
					'maxValue' => '99999'
				)));

			if ($newPin !== $repeatPin)
				throw new \Exception(_('El nuevo PIN enviado no coincide en los valores suministrados. Escriba ambos campos nuevamente e intente de nuevo.'));

			// if ($userModel->checkPin($this->session->user->userId, $currentPin) === false)
			// 	throw new \Exception(_('El PIN actual suministrado no coincide con el PIN almacenado'));

			$userModel->updatePin($this->session->user->userId, $newPin);

			$this->sendSuccess('OK');
		}
		catch (Exception $e)
		{
			$this->sendError($e->getMessage());
		}		
	}

	public function setInitialPinAction()
	{
		$userModel = new UserModel();

		try
		{
			CsrfUtil::validateForm(true);

			$newPin = $this->post('newPin', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'int',
					'maxValue' => '99999'
				)));

			$repeatPin = $this->post('repeatPin', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'int',
					'maxValue' => '99999'
				)));

			if ($newPin !== $repeatPin)
				throw new \Exception(_('El nuevo PIN enviado no coincide en los valores suministrados. Escriba ambos campos nuevamente e intente de nuevo.'));
			
			if ($userModel->isPinSet($this->session->user->userId) === true)
				throw new \Exception(_('El PIN ya se encuentra asignado'));

			$userModel->updatePin($this->session->user->userId, $newPin);

			$this->sendSuccess('OK');
		}
		catch (Exception $e)
		{
			$this->sendError($e->getMessage());
		}		
	}

	public function generateBingoAction()
	{
		try
		{
			$securityModel = new SecurityModel();

			$viewVars = array();
			$viewVars['bingoFields'] = $securityModel->generateBingoFieldsByUserId($this->session->user->userId);

			$bingoView = new PdfView();
			$bingoView->outputFilename = "bingo_digo.pdf";
			$bingoView->render($viewVars, null, 'Security/bingo.phtml');
		}
		catch (Exception $e)
		{
			$this->renderView(array(), 'Security/error.phtml');
		}
	}
}
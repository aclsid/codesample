<?php

class SecurityModel extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function generateBingoFieldsByUserId($userId)
	{
		try
		{
			$this->db->beginTransaction();

			//We first check if this user had any other Bingos and mark them as inactive
			$sql = "UPDATE `Bingo` SET `active` = 0 WHERE `userId` = :userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
			$stmt->execute();

			//We create the main value for this Bingo first
			$sql = "INSERT INTO 
						`Bingo` (
							`userId`, 
							`expirationDate` 
						) 
					VALUES (
							:userId, 
							:expirationDate
					)";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $userId, \PDO::PARAM_STR);		
			$stmt->bindValue(':expirationDate', DateUtil::getDateMonthsFromNow(2)->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
			$stmt->execute();

			$bingoId = $this->db->lastInsertId();

			//We then prepare the statement that will be used to add Bingo values
			$sql = "INSERT INTO 
					`BingoValue` (
						`bingoId`, 
						`column`,
						`row`,
						`value` 
					) 
					VALUES (
							:bingoId, 
							:column,
							:row,
							:value
					)";
			$stmt = $this->db->prepare($sql);

			//Generate 7x7 matrix with random values of 5 digits.
			$bingoFields = array();

			//Note that for presentation, columns will be converted to letters from A to G
			for ($column = 1; $column < 8; $column++)
			{
				for ($row = 1; $row < 8; $row++)
				{
					//NOTE: The generation of the row and columns matrix goes by row first and then switches to the next column
					$bingoFields[$row][$column] = rand(1,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);

					$stmt->bindParam(':bingoId', $bingoId, \PDO::PARAM_INT);
					$stmt->bindParam(':column', $column, \PDO::PARAM_INT);
					$stmt->bindParam(':row', $row, \PDO::PARAM_INT);
					$stmt->bindValue(':value', PasswordUtil::encrypt($bingoFields[$row][$column]), \PDO::PARAM_STR);
					$stmt->execute();
				}
			}

			$this->db->commit();

			//Return newly created matrix so the view can generate a PDF
			return $bingoFields;
		}
		catch (Exception $e)
		{
			$this->db->rollback();
			throw new Exception(_("No se ha podido generar el Bingo correctamente"));
		}
	}

	//Returns a single hashed value with row and column info
	//that should feed a user prompt
	public function getRandomBingoValueByUserId($userId)
	{
		$sql = "SELECT 
					bv.`bingoValueId`,
				    b.`bingoId`,
				    bv.`column`, 
				    bv.`row`, 
				    bv.`value`
				FROM
				    `BingoValue` AS bv
				LEFT JOIN
				    `Bingo` AS b ON b.`bingoId` = bv.`bingoId`
				WHERE
					b.`userId` = :userId AND b.`active` = TRUE AND bv.`used` = FALSE";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
		$stmt->execute();

		$bingoValueCollection = array();

		while ($bingoValue = $stmt->fetchObject('BingoRandomValueEntity'))
			$bingoValueCollection[] = $bingoValue;

		//We now mark that value as used in the database
		//Note the -1 in the count because our collection starts with zero 
		$randomBingoValue = $bingoValueCollection[rand(0, count($bingoValueCollection) - 1)];

		$sql = "UPDATE `BingoValue` SET `used` = TRUE WHERE `bingoValueId` = :bingoValueId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':bingoValueId', $randomBingoValue->bingoValueId, \PDO::PARAM_INT);
		$stmt->execute();

		//We calculate the remaining values that are still valid
		$randomBingoValue->valuesRemaining = count($bingoValueCollection) - 1;

		return $randomBingoValue; //BingoRandomValueEntity
	}

	//Checks wether a user has generated a Bingo and that it has valid values left
	//and also that the expiration date hasn't passed
	public function hasValidBingo($userId)
	{
		$sql = "SELECT 
					b.`expirationDate`
				FROM
				    `BingoValue` AS bv
				LEFT JOIN
				    `Bingo` AS b ON b.`bingoId` = bv.`bingoId`
				WHERE
					b.`userId` = :userId AND b.`active` = TRUE AND bv.`used` = FALSE";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
		$stmt->execute();

		$collection = $stmt->fetchAll(\PDO::FETCH_OBJ);

		if (empty($collection))
			return false;

		if (DateUtil::checkDateHasPassed($collection[0]->expirationDate))
			return false;

		return true;
	}
}
<?php

use PHPUnit\Framework\TestCase;

class AuthenticationModelTest extends TestCase
{

	public function testSaveLoginTrack()
	{
		$authenticationModel = new AuthenticationModel();
		$userModel = new UserModel();

		self::deleteRecord(1000);
		$userModel->unblockUser(1000);

		$authenticationModel->failAttempt(1000);
		$authenticationModel->checkLoginIp(1000);

		$sql = "SELECT
					`lastLogin`,
					`lastBadAttempt`,
					`lastLoginIp`,
					`badAttempt`,
					`ipChange`
				WHERE
					`userId` = :userId";

		$stmt = $authenticationModel->db->prepare($sql);
		$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
		$stmt->execute();

		$obj = $stmt->fetch(\PDO::FETCH_OBJ);

		$obj->lastBadAttempt = date_format(date_create($obj->lastBadAttempt),"Y-m-d");
		$obj->lastLogin = date_format(date_create($obj->lastLogin),"Y-m-d");

		$expected->lastLoginIp = ip2long(filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP));	
		$expected->badAttempt = 1;
		$expected->lastLogin = date("Y-m-d");
		$expected->lastBadAttempt = date("Y-m-d");
		$expected->ipChange = 0;

		$this->assertTrue($obj->lastLogin === $expected->lastLogin && $obj->lastBadAttempt === $expected->lastBadAttempt && $obj->lastLoginIp === $expected->lastLoginIp && $obj->badAttempt === $expected->badAttempt && $obj->ipChange === $expected->ipChange);

	}

	public function testUserBlockingBy3recentBadLoginAttempts()
	{

		$authenticationModel = new AuthenticationModel();
		$userModel = new UserModel();

		self::deleteRecord(1000);
		$userModel->unblockUser(1000);

		$authenticationModel->failAttempt(1000); //primer intento
		$authenticationModel->failAttempt(1000); //segundo intento
		$authenticationModel->failAttempt(1000); //tercer intento

		//el usuario debe estar bloqueado
		$this->assertEquals(
            UserModel::BLOCKED_USER,
            $userModel->getUserStatus($userId)
        );
	}

	public function testActiveUserBy3UnrelatedBadLoginAttempts()
	{

		$authenticationModel = new AuthenticationModel();
		$userModel = new UserModel();

		self::deleteRecord(1000);
		$userModel->unblockUser(1000);

		$authenticationModel->failAttempt(1000); //primer intento
		//cambiar fecha del registro, simulando que ocurrio hace un dia respecto al siguiente
		self::changeRecordDate('lastBadAttempt', '-1 days', 1000);

		$authenticationModel->failAttempt(1000); //segundo intento
		//cambiar fecha del registro, simulando que ocurrio hace un dia respecto al siguiente
		self::changeRecordDate('lastBadAttempt', '-1 days', 1000);

		$authenticationModel->failAttempt(1000); //tercer intento

		//el usuario debe estar activo
		$this->assertEquals(
            UserModel::ACTIVE_USER,
            $userModel->getUserStatus($userId)
        );
	}

	public function testUserBlockingBy3recentIpChanges()
	{

		$authenticationModel = new AuthenticationModel();
		$userModel = new UserModel();

		self::deleteRecord(1000);
		$userModel->unblockUser(1000);

		$authenticationModel->checkLoginIp(1000); //primer intento
		//se simulan diferentes ips
		self::changeRecordLastLoginIp(1, 1000);

		$authenticationModel->checkLoginIp(1000);	//segundo intento

		//se simulan diferentes ips
		self::changeRecordLastLoginIp(2, 1000);

		$authenticationModel->checkLoginIp(1000);	//tercer intento

		//el usuario debe estar bloqueado
		$this->assertEquals(
            UserModel::BLOCKED_USER,
            $userModel->getUserStatus($userId)
        );

	}

	public function testActiveUserBy3UnrelatedIpChanges()
	{

		$authenticationModel = new AuthenticationModel();
		$userModel = new UserModel();

		self::deleteRecord(1000);
		$userModel->unblockUser(1000);

		$authenticationModel->checkLoginIp(1000); //primer intento
		//cambiar fecha del registro, simulando que ocurrio hace un dia respecto al siguiente
		self::changeRecordDate('lastLogin', '-1 days', 1000);
		//se simulan diferentes ips
		self::changeRecordLastLoginIp(1, 1000);

		$authenticationModel->checkLoginIp(1000); //segundo intento
		//cambiar fecha del registro, simulando que ocurrio hace un dia respecto al siguiente
		self::changeRecordDate('lastLogin', '-1 days', 1000);
		//se simulan diferentes ips
		self::changeRecordLastLoginIp(2, 1000);

		$authenticationModel->checkLoginIp(1000); //tercer intento

		//el usuario debe estar activo
		$this->assertEquals(
            UserModel::ACTIVE_USER,
            $userModel->getUserStatus($userId)
        );
	}

	public function testBadAttemptCounter()
	{
		$authenticationModel = new AuthenticationModel();
		$userModel = new UserModel();

		self::deleteRecord(1000);
		$userModel->unblockUser(1000);

		$attempt[1] = $authenticationModel->failAttempt(1000);
		$attempt[2] = $authenticationModel->failAttempt(1000);

		self::changeRecordDate('lastBadAttempt', '-1 days', 1000);

		$attempt[3] = $authenticationModel->failAttempt(1000); 
		$attempt[4] = $authenticationModel->failAttempt(1000); 
		$attempt[5] = $authenticationModel->failAttempt(1000);

		$userModel->unblockUser(1000);

		$attempt[6] = $authenticationModel->failAttempt(1000);  

		self::changeRecordDate('lastBadAttempt', '-1 days', 1000);

		$attempt[7] = $authenticationModel->failAttempt(1000); 
		$attempt[8] = $authenticationModel->failAttempt(1000); 

		$expectAttempt[1] = 1;
		$expectAttempt[2] = 2;
		$expectAttempt[3] = 1;
		$expectAttempt[4] = 2;
		$expectAttempt[5] = 3;
		$expectAttempt[6] = 1;
		$expectAttempt[7] = 1;
		$expectAttempt[8] = 2;
		//los intentos fallidos deben coincidir
		$this->assertTrue($expectAttempt === $attempt);

	}

	public function changeRecordDate($type, $subDate, $userId)
	{
		$authenticationModel = new AuthenticationModel();

		$date = $date("Y-m-d H:i:s",strtotime($subDate));

		if ($type === 'lastLogin') {

			$sql = "UPDATE 
					`LoginTrack`
				SET
					`lastLogin` = :lastLogin
				WHERE
					 `userId` = :userId";

			$stmt = $authenticationModel->db->prepare($sql);
			$stmt->bindParam(':lastLogin', $date, \PDO::PARAM_STR);
		}
		elseif ($type === 'lastBadAttempt') {

			$sql = "UPDATE 
					`LoginTrack`
				SET
					`lastBadAttempt` = :lastBadAttempt
				WHERE
					 `userId` = :userId";

			$stmt = $authenticationModel->db->prepare($sql);
			$stmt->bindParam(':lastBadAttempt', $date, \PDO::PARAM_STR);
		}

		$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
		$stmt->execute();
	}

	public function changeRecordLastLoginIp($ip, $userId)
	{
		$authenticationModel = new AuthenticationModel();

		$sql = "UPDATE 
					`LoginTrack`
				SET
					`lastLoginIp` = :lastLoginIp
				WHERE
					 `userId` = :userId";

			$stmt = $authenticationModel->db->prepare($sql);
			$stmt->bindParam(':lastLoginIp', $ip, \PDO::PARAM_STR);
			$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
			$stmt->execute();
	}

	public function deleteRecord($userId)
	{
		$authenticationModel = new AuthenticationModel();

		$sql = 'DELETE FROM
					`LoginTrack`
				WHERE
					`userId` = :userId';

		$stmt = $authenticationModel->db->prepare($sql);
		$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
		$stmt->execute();
	}

}


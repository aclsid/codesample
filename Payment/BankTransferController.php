<?php 

class BankTransferController extends Controller 
{
	public function indexAction(array $routeParams)
	{
		$this->renderView(array(), 'Transfer/index.phtml');
	}

	public function createAction(array $routeParams)
	{ 

		$bankAccountModel = new BankAccountModel();
		$viewVars = array();

		$ajax = false;

		if ($_POST) 
		{
			try
			{
				CsrfUtil::validateForm();

				$transfer = new BankTransferEntity();

				$ajax = $this->post('ajax', new RequestOptionsEntity(array(
					'dataType' => 'bool'
				)));

				$transfer->sourceBankAccountId = $this->post('sourceBankAccountId', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'int'
				)));

				$transfer->destinationBankAccountId = $this->post('destinationBankAccountId', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'int'
				)));

				$transfer->transferCode = $this->post('transferCode', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'string'
				)));

				$transfer->transferAmount = $this->post('transferAmount', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'float_es',
					'maxValue' => 9999999, //The maximum value is 9.999.999,99 as determined by the DB
				)));

				$transfer->transferDate = $this->post('transferDate', new RequestOptionsEntity(array(
					'required' => true,
					'dataType' => 'date_es'
				)));

				$transfer->userId = $this->session->user->userId;
				$bankTransferModel = new BankTransferModel();
				$transfer = $bankTransferModel->saveTransfer($transfer);
				
				//reporte de transferencia enviado por correo
				$mailLinkModel = new MailLinkModel();
				
				$correo = new MailEntity;
				$correo->to = array('servicio@digopago.com'=>_('Sistema Digo'));
				$correo->from = array('servicio@digopago.com'=>_('Sistema Digo')); 
				$correo->subject = _('Reporte de transferencia'); 
				$emailView = new EmailView;
				$correo->body = $emailView->getBody(array("user" => $transfer->userId, "bankTransferId" => $transfer->bankTransferId), 'Mail/transferMail.phtml', $correo);
				MailUtil::send($correo);

				if ($ajax)
					$this->sendSuccess('Ok');
				else
					header('Location:/transferencia');
			}
			catch (Exception $e)
			{
				if ($ajax)
				{
					$this->sendError($e->getMessage());
					return;
				}
				else
					$viewVars['pageError'] = $e->getMessage();
			}
		}

		$viewVars['userBankAccounts'] = $bankAccountModel->getBankAccountsByUserId($this->session->user->userId);
		$viewVars['systemBankAccounts'] = $bankAccountModel->getSystemBankAccounts();

		$this->renderView($viewVars, 'Transfer/edit.phtml');
	}

	public function overviewAction(array $routeParams)
	{
		// $current = $this->post('current', false, null, 1);
		$current = $this->post('current', new RequestOptionsEntity(array(
			'required' => false,
			'dataType' => 'int',
			'defaultValue' => 1,
		)));

		$rowCount = $this->post('rowCount', new RequestOptionsEntity(array(
			'required' => false,
			'dataType' => 'int',
			'defaultValue' => 50,
		)));

		$sort = $this->postArray('sort', false);

		$searchPhrase = $this->post('searchPhrase', new RequestOptionsEntity(array(
			'required' => false,
			'dataType' => 'string'
		)));

		$bankTransferModel = new BankTransferModel();

		$this->sendSuccess(array(
			'current' => $current,
			'rowCount' => $rowCount,
			'rows' => $bankTransferModel->getTableContent($current, $rowCount, $sort, $searchPhrase, false, $this->session->user->userId),
			'total' => $bankTransferModel->getTableContent($current, $rowCount, $sort, $searchPhrase, true, $this->session->user->userId),
		));
	}

	public function detailAction(array $routeParams)
	{
		$viewVars = array();
		//recibe el ID
		$bankTransferId = (int)$routeParams['bankTransferId'];
		$bankTransferModel = new BankTransferModel();

		$viewVars['bankTransfer'] = $bankTransferModel->getBankDetailsByBankTransferId($bankTransferId, $this->session->user->userId);
		if ($viewVars['bankTransfer']->status === 1) {
			
			$transactionModel = new TransactionModel();

			$viewVars['transaction'] = $transactionModel->getTransactionDetailsBytransactionId($viewVars['bankTransfer']->transactionId, $this->session->user->userId);			
		}
		
		$this->renderView($viewVars, 'Transfer/detail.phtml');
	}
}
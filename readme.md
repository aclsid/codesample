###Sample Code Repo###


##Payment##
A collection of files that show the architecture of a payment system I developed. Notice that in the controller a view can take the form of web output, or mail view or even a PDF one. Also a set of request processing functions were done at the base controller level to sanitize and format input that are reused everywhere.

Also notice in the model file, how ORMs are explicitly avoided, in large part because some of them like Laravel's do not support composite keys, in other cases, they tend to lose database specific optimizations like MySQL spatial functions or INSERT ON DUPLICATE KEY, aside from the fact that the code is preparing statements and reusing them by changing the binded parameters.

Also included is a test file that shows how PHPUnit is being used for unit tests.

##AjaxTs##
A small and one of my very first scripts on Typescript that provided a way to encapsulate Ajax so that there wasn't any need for jQuery Ajax or similar.

##ChartTs##
Another typescript code sample that was used to showcase a jQuery plot component on a dashboard once the user logged in.

##Migration##
Database migration system written from scratch and part of my utility belt that I use everywhere. Aside from being extremely useful, it has a neat dependency calculation system based on a topological sort. I tend to upgrade it with whatever new ideas come out, and after seeing the Laravel migrations system go back or reset functionality, I have a new skill to teach the script.

##Wechat##
A Wechat (Chinese Whatsapp) third-party authentication system. This is by far the toughest code I have had to write. I thought this was going to be another Facebook/Amazon Marketplace API kind of thing, but most of the good documentation was in Chinese. The system itself was also pushing the envelope because it made it easier for other people to create Wechat applications once they selected us as a third-party provider. That way we could provide menus, chatbots etc for other people's accounts. The main project was about a location engagement system using bluetooth beacons, and with Wechat, all you had to do was shake your phone while at the location and you were able to access the location Wechat application that among other things, could allow you to order food, pay through Wechat Pay, etc.
<?php namespace Application\Model;

use Application\Utils\Url;
use Application\Utils\MemoryCache;
use Application\Utils\Rest;
use Application\Utils\Logger;

use Application\Model\LocationModel;
use Application\Model\MessageModel;
use Application\Model\MessagingMessageModel;
use Application\Model\StoryModel;
use Marketingcenter\Model\UserModel;
use Application\Model\WechatBotModel;
use Application\Model\WechatModel;
use Application\Model\UserActualLocationModel;
use Application\Model\VisitModel;
use Application\Model\OrganizationModel;

use Application\Entity\WechatUserEntity;
use Application\Entity\WechatWifiConnectEntity;
use Application\Entity\WechatBotUserSessionEntity;
use Application\Entity\MessagingMessageEntity;


use Pheanstalk\Pheanstalk;

include_once ROOT_PATH. '/vendor/phpWechat/wxBizMsgCrypt.php';

/**
 * Class WechatThirdPartyModel
 * @package Application\Model
 */
class WechatThirdPartyModel extends Model
{
	/**
	 * WechatThirdPartyModel
	 *
	 * @var
     */
	public $appId;
	/**
	 * WechatThirdPartyModel
	 *
	 * @var
     */
	public $componentVerifyTicket;
	/**
	 * WechatThirdPartyModel
	 *
	 * @var
     */
	public $accessToken;
	/**
	 * WechatThirdPartyModel
	 *
	 * @var
     */
	public $preAuthCode;

	const EVENT_SUBSCRIBE = 'subscribe';
	const EVENT_UNSUBSCRIBE = 'unsubscribe';
	const EVENT_WIFICONNECT = 'WifiConnected';
	const EVENT_CLICK = 'CLICK';
	const EVENT_LOCATION = 'LOCATION';

	/**
	 * WechatThirdPartyModel constructor.
     */
	public function __construct()
	{
		parent::__construct();
		$this->setPreAuthCode();
	}

	/**
	 * setPreAuthCode
	 * @throws \Exception
     */
    public function setPreAuthCode()
	{
		$this->appId = $this->environment->wechatThirdPartyAccount->appId;

		$this->componentVerifyTicket = MemoryCache::get('componentVerifyTicket');
		if ($this->componentVerifyTicket === false)
		{
			$componentVerifyTicket = null;
			if (file_exists(ROOT_PATH.'/data/wechat_third_party/component_verify_ticket.dat'))
				$componentVerifyTicket = file_get_contents(ROOT_PATH.'/data/wechat_third_party/component_verify_ticket.dat');
			
			if ($componentVerifyTicket == null || $componentVerifyTicket == '')
				throw new \Exception('Could not get a component verify ticket from file');

			Logger::wechatThirdPartyLog('Loaded component verify ticket from file: '.$componentVerifyTicket);
			file_put_contents(ROOT_PATH.'/data/wechat_third_party/component_verify_ticket.dat', $componentVerifyTicket);
			MemoryCache::set('componentVerifyTicket', $componentVerifyTicket, 660);
		}

		/* ACCESS TOKEN
		 * ============
		 */
		$this->accessToken = MemoryCache::get('accessToken');

		Logger::wechatThirdPartyLog('Memcached Component Access Token: '.$this->accessToken);

		if ($this->accessToken === false || $this->accessToken === null)
		{
			$params = array(
				'component_appid' => $this->environment->wechatThirdPartyAccount->appId, 
				'component_appsecret' => $this->environment->wechatThirdPartyAccount->appSecret,
				'component_verify_ticket' => $this->componentVerifyTicket,
			);

			//Params must be sent as JSON. See docs at https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1419318587&token=&lang=zh_CN
			$response = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/component/api_component_token', json_encode($params));

			$response = json_decode($response);

			MemoryCache::set('accessToken', $response->component_access_token, $response->expires_in);
			$this->accessToken = $response->component_access_token;

			Logger::wechatThirdPartyLog('New Component Access Token: '.$response->component_access_token);
		}

		/* PREAUTH CODE
		 * ============
		 */
		$this->preAuthCode = MemoryCache::get('preAuthCode');
		// $this->preAuthCode = false;
		if ($this->preAuthCode === false || $this->preAuthCode === null)
		{
			$params = array(
				'component_appid' => $this->environment->wechatThirdPartyAccount->appId,
			);

			$response = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/component/api_create_preauthcode?component_access_token='.$this->accessToken, json_encode($params));
			$response = json_decode($response);

			MemoryCache::set('preAuthCode', $response->pre_auth_code, $response->expires_in);

			$this->preAuthCode = $response->pre_auth_code;

			Logger::wechatThirdPartyLog('API Create PreAuth Code: '.$response->pre_auth_code);
		}
	}

	/**
	 * response
	 *
	 * @param $event
	 * @param $authorizerAppId
	 * @param $nonce
	 * @param $timestamp
	 *
     * @return string
     */
    public function response($event, $authorizerAppId, $nonce, $timestamp)
	{
		$wechatBotModel = new WechatBotModel($event, $authorizerAppId);

		if ($wechatBotModel->userLanguage == UserModel::LANG_ZH)
			$this->setLanguage('zh_CN');
		else
			$this->setLanguage('en_US');


		/*$botUserSession = MemoryCache::get('botUserSession'.$event->fromUserName);
		if (!$botUserSession && !$botUserSession->languageBot) {
			$botUserSession = new WechatBotUserSessionEntity();
			$botUserSession->languageBot = new \stdClass();
			$botUserSession->languageBot->step = 1;
		} else {
			$botUserSession = unserialize($botUserSession);
		}


		if (isset($botUserSession->languageBot) || strtolower(trim($event->content)) == '开始') {
			if (strtolower(trim($event->content)) == '开始') {
				$botUserSession->languageBot = new \stdClass();
				$step = $botUserSession->languageBot->step = 1;
			} else {
				$step = $botUserSession->languageBot->step;
			}

			// language bot message for testing
			$messages = $wechatBotModel->processLanguageMessage($botUserSession, $event);
			if ($messages) {
				if (isset($messages['ready']) && $messages['ready']) {
					if (strtolower(trim($event->content)) == 'ready') {
						// user input ready
						$readyMessages = $messages['ready'];
						foreach ($readyMessages as $readyMessage) {
							if (substr($readyMessage, 0, 10) == '#sendimage') {
								$item = array(
									"touser" => $event->fromUserName,
									"msgtype" => "image",
									"image" => array(
										"media_id" => substr($readyMessage, 11),
									),
								);

								$jsonItem = json_encode($item, JSON_UNESCAPED_UNICODE);
								$this->sendRichMediaMessageToUser($authorizerAppId, $jsonItem);
							} else {
								$this->sendMessageToUser($authorizerAppId, $event->fromUserName, $readyMessage);
							}
						}

						$botUserSession->languageBot->step = $step + 1;
						MemoryCache::set('botUserSession'.$event->fromUserName, serialize($botUserSession), 600);
					} else {
						// user input anything, but ready
						$notReadyMessages = $messages['not_ready'];
						foreach ($notReadyMessages as $notReadyMessage) {
							$this->sendMessageToUser($authorizerAppId, $event->fromUserName, $notReadyMessage);
						}
					}

				} else {
					// normal
					foreach ($messages as $message) {
						if (substr($message, 0, 10) == '#sendimage') {
							$item = array(
								"touser" => $event->fromUserName,
								"msgtype" => "image",
								"image" => array(
									"media_id" => substr($message, 11),
								),
							);

							$jsonItem = json_encode($item, JSON_UNESCAPED_UNICODE);
							$this->sendRichMediaMessageToUser($authorizerAppId, $jsonItem);
						} else if (substr($message, 0, 10) == '#sendvoice') {
							$item = array(
								"touser" => $event->fromUserName,
								"msgtype" => "voice",
								"voice" => array(
									"media_id" => substr($message, 11),
								),
							);

							$jsonItem = json_encode($item, JSON_UNESCAPED_UNICODE);
							$this->sendRichMediaMessageToUser($authorizerAppId, $jsonItem);
						} else if (substr($message, 0, 11) == '#sendmpnews') {
							$item = array(
								"touser" => $event->fromUserName,
								"msgtype" => "mpnews",
								"mpnews" => array(
									"media_id" => substr($message, 12),
								),
							);

							$jsonItem = json_encode($item, JSON_UNESCAPED_UNICODE);
							$this->sendRichMediaMessageToUser($authorizerAppId, $jsonItem);
						} else {
							$this->sendMessageToUser($authorizerAppId, $event->fromUserName, $message);
						}
					}

					$botUserSession->languageBot->step = $step + 1;
					MemoryCache::set('botUserSession'.$event->fromUserName, serialize($botUserSession), 600);
				}
			}
		} else {*/
			$message = $wechatBotModel->response();
			Logger::wechatThirdPartyLog('Wechat TP Platform Bot Response: '.$message);

			// Logger::wechatThirdPartyLog('DEBUG Wechat Crypt Token: '.$token.PHP_EOL.'Encoding AES Key: '.$encodingAesKey.' '.$authorizerAppId);

			$pc = new \WXBizMsgCrypt(
				$this->environment->wechatThirdPartyAccount->token,
				$this->environment->wechatThirdPartyAccount->encodingAesKey,
				$this->environment->wechatThirdPartyAccount->appId
			);

			$encryptedMessage = '';
			$errCode = $pc->encryptMsg($message, $timestamp, $nonce, $encryptedMessage);

			// $encryptedMessage = $wechatMessageCrypt->encrypt($message);
			// Logger::wechatThirdPartyLog('newsEvents Encrypted response XML: '.$encryptedMessage);
			// Logger::wechatThirdPartyLog('Response: '.$encryptedMessage.PHP_EOL.'Error Code:'.$errCode);

			return $encryptedMessage;
		//}
	}

	/**
	 * processEvent
	 * @param $event
	 * @param $authorizerAppId
	 *
	 * @throws \Exception
	 */
	public function processEvent($message, $authorizerAppId)
	{
		$locationModel = new LocationModel();
		$wechatModel = new WechatModel();


		switch ($message->event)
		{
			case self::EVENT_UNSUBSCRIBE:
				$this->deleteUser($message->fromUserName);
				// must delete cache for wx_openId
				MemoryCache::delete('wx_'. $message->fromUserName);

				return;
				break;
			case self::EVENT_SUBSCRIBE:
				$location = null;

				if ($wechatModel->checkWifiConnectUser($message->fromUserName) !== false) {
                    $location = $wechatModel->getLocationByWifiConnectOpenId($message->fromUserName);
                    if (isset($location)) {
                    	//set the WechatWifiSubscriber
                    	$wechatModel->setUserWechatWifiSubscriber($message->fromUserName, $location->locationId);
                    }
                }
			

				$this->processBotMessage($message, $authorizerAppId, $location);

				break;
			case self::EVENT_WIFICONNECT:

				$location = $locationModel->getLocationByWechatShopId($message->shopId);

				//Move this to a separate function to prefill this entity object
				//Could be the constructor for the Entity object itself that accepts default values when nothing is passed
				$wifiConnect = new WechatWifiConnectEntity();
				$wifiConnect->openId = $message->fromUserName;
				$wifiConnect->locationId = isset($location->locationId) ? $location->locationId : null;
				$wifiConnect->appId = $authorizerAppId;
				$now = new \DateTime('now');
				$wifiConnect->dateCreated = $now->format('Y-m-d H:i:s');
				$wechatModel->saveWifiConnectEvent($wifiConnect);

				Logger::wechatThirdPartyLog('WifiConnectEvent: '.serialize($wifiConnect).PHP_EOL.'Location: '.serialize($location));

				//Ensure that this false is working by modifying the getLocationByWechatShopId function if needed
				//There should be no reason why $location should be null
				if ($location === false)
				{
					$this->sendMessageToUser($authorizerAppId, $message->fromUserName, _('Welcome to Magnet, please share your location to get started'));
					return;
				}


				// if ($wechatModel->checkAccountExists($message->fromUserName) === false)
				// {
				// 	//Add a job that will fetch the details for the empty Magnet user about to be created. If the user details return empty, rerun the job one more time.

				// 	$pheanstalk = new Pheanstalk('127.0.0.1');

				// 	$jobData = array(
				// 		'openId' => $message->fromUserName,
				// 		'organizationId' => $organizationId,
				// 		'lang' => UserModel::LANG_EN
				// 	);

				// 	$pheanstalk
				// 	  ->useTube('default')
				// 	  ->put(json_encode($jobData));

				// 	// $user = $wechatModel->getUserProfileEx($message->fromUserName, $location->organizationId, 1);

				// 	$location = $wechatModel->getLocationByWifiConnectOpenId($message->fromUserName);
				// 	$location !== false ? $location : null; 
				// }

				$this->processBotMessage($message, $authorizerAppId, $location);
				break;
			case self::EVENT_CLICK:
				// use this function must have location id
				$botUserSession = MemoryCache::get('botUserSession'.$message->fromUserName);
				$botUserSession = unserialize($botUserSession);

				$eventKey = $message->eventKey;
                if (substr($eventKey, 0, 2) == 'EB') {
                    $ebId = substr($eventKey, 3);
                    
                    $storyModel = new StoryModel();
                    $story = $storyModel->getStoryByStoryId($ebId);

                    $startModuleId = $story->startModuleId;

					if (!isset($botUserSession->selectedLocationId)) {
						// get org info
						$orgModel = new OrganizationModel();
						$org = $orgModel->getOrganizationById($story->organizationId);

    					$this->sendMessageToUser($authorizerAppId, $message->fromUserName, _('You need to visit a '. $org->organizationName. ' location to use this feature.'));
    					return;
    				}

                    /*$buttonLabel = $story->buttonLabel;

                    $this->sendMessageToUser($authorizerAppId, $message->fromUserName, $buttonLabel);

                    if (isset($botUserSession) && $botUserSession->userId && $botUserSession->selectedLocationId && $buttonLabel) {
                    	$messagingMessageModel = new MessagingMessageModel();
						$messagingMessageModel->serveMessage($botUserSession, $buttonLabel);
		        	}*/

                    if ($startModuleId) {
                    	// get story module info
                    	$storyModule = $storyModel->getStoryModuleWithOptionsByStoryModuleId($startModuleId);

                		if ($storyModule->inputTypeId == CustomBotModel::INPUT_TYPE_NONE 
                			&& $storyModule->destinationModuleId) {
                			// input type is none     
                    		$this->processStepBot($storyModule->moduleId, $botUserSession, $authorizerAppId, $message);
                		} else if ($storyModule->inputTypeId == CustomBotModel::INPUT_TYPE_BUTTONS) {
                			// input type is buttons
	                		if (isset($storyModule->options)) {
	                			$responseContent .= "\n\n";
	                			$optionsCnt = count($storyModule->options);
	                			foreach ($storyModule->options as $key => $option) {
	                				$responseContent .= WechatBotModel::$emojis[$key + 1]. ' '. $option->label;
                					if ($key + 1 < $optionsCnt) {
	                					$responseContent .= "\n";
                					}
	                			}

		                		// remeber current module and option in session and memcache
	        					if (!$botUserSession) {
									$botUserSession = new WechatBotUserSessionEntity();
	        					}

	            				$botUserSession->storyModule = $storyModule;
	            				$botUserSession->languageBot = null;
	    						MemoryCache::set('botUserSession'. $message->fromUserName, serialize($botUserSession), 600);
	                		}


	        				Logger::wechatThirdPartyLog('WechatTP Event Response Location: '.serialize($responseContent));

							$this->sendMessageToUser($authorizerAppId, $message->fromUserName, $responseContent);

							if (isset($botUserSession) && isset($botUserSession->userId) && $botUserSession->selectedLocationId && $responseContent) {
								$messagingMessageModel = new MessagingMessageModel();
								$messagingMessageModel->serveMessage($botUserSession, $responseContent);
				        	}
				        }
                    }
                } else {
					if (!isset($botUserSession->selectedLocationId)) {
	    				$this->sendMessageToUser($authorizerAppId, $message->fromUserName, _('To access this function please share your location first.'));
	    				return;
    				}
                }

				break;
			case self::EVENT_LOCATION:
				// User report the location
				$botUserSession = MemoryCache::get('botUserSession'.$message->fromUserName);
				$botUserSession = unserialize($botUserSession);

				if (!$botUserSession) {
					$botUserSession = new WechatBotUserSessionEntity();
				}

				// get location
				$lat = $message->latitude;
				$long = $message->longitude;
				$pre = $message->precision;

				$closestLocation = $locationModel->getClosestLocationByLatLong($lat, $long);
				Logger::wechatThirdPartyLog('closest location:'. serialize($closestLocation));

				if ($closestLocation) {
					$botUserSession->selectedLocationId = $closestLocation->location_id;
					
				}
				$botUserSession->wechatId = $message->fromUserName;
				MemoryCache::set('botUserSession'. $message->fromUserName, serialize($botUserSession), 600);

				break;
		}
	}

	private function processStepBot($moduleId, $botUserSession, $authorizerAppId, $message)
	{
        $storyModel = new StoryModel();

		$storyModule = $storyModel->getStoryModuleWithOptionsByStoryModuleId($moduleId);
    	$responseContent = $storyModule->responseContent;

    	Logger::wechatThirdPartyLog('WechatTP Event Response Location: '.serialize($responseContent));

		if ($this->is_json($responseContent)) {
			// wechat media is json format
    		$arr = json_decode($responseContent);

			$mediaId = $arr->media_id;
			$mediaType = $arr->media_type;
			$this->sendMessageToUser($authorizerAppId, $message->fromUserName, $mediaId, $mediaType);

    	} else {
			$this->sendMessageToUser($authorizerAppId, $message->fromUserName, $responseContent);
    	}

		if (isset($botUserSession) && $botUserSession->userId && $botUserSession->selectedLocationId && $responseContent) {
			$messagingMessageModel = new MessagingMessageModel();
			$messagingMessageModel->serveMessage($botUserSession, $responseContent);
    	}

    	if ($storyModule->inputTypeId == CustomBotModel::INPUT_TYPE_NONE 
    		&& $storyModule->destinationModuleId) {
    		sleep(1);
    		$this->processStepBot($storyModule->destinationModuleId, $botUserSession, $authorizerAppId, $message);
    	} else {
    		// end none
    		// remeber current module and option in session and memcache
			if (!$botUserSession) {
				$botUserSession = new WechatBotUserSessionEntity();
			}

			$botUserSession->storyModule = $storyModule;
			MemoryCache::set('botUserSession'. $message->fromUserName, serialize($botUserSession), 600);

    	}
	}


	public function processBotMessage($message, $authorizerAppId, $location)
	{
		$userActualLocationModel = new UserActualLocationModel;
		$visitModel = new VisitModel();
		$messageModel = new MessageModel();

		Logger::wechatThirdPartyLog('WechatTP Event Response Location: '.serialize($location));

		$wechatBotModel = new WechatBotModel($message, $authorizerAppId, $location);

		if ($wechatBotModel->userLanguage == UserModel::LANG_ZH)
			$this->setLanguage('zh_CN');
		else
			$this->setLanguage('en_US');

		$userActualLocationModel->checkIn($wechatBotModel->user->userId, $location->locationId);
		$visitModel->recordVisit($wechatBotModel->user->userId, $location->locationId);

		if ($wechatBotModel->user->fullname) {
			$response = $wechatBotModel->setLocationStateByLocationId($location->locationId);

			$items = $wechatBotModel->prepareJSONRichMediaMessage($response->items, $location);

			Logger::wechatThirdPartyLog('Wifi JSON Response: '.$items);

			$this->sendRichMediaMessageToUser($authorizerAppId, $items);

			$messageModel->sendTextToUser1($wechatBotModel->user->userId, $location->locationId, _('Displayed location details for '.$location->locationName));
		}

		Logger::wechatThirdPartyLog('Processed Wifi Event successfully for App ID: '.$authorizerAppId.' User:'.$message->fromUserName);
	}

	/**
	 * sendLocationDetails
     */
    public function sendLocationDetails()
	{

	}

	/**
	 * generateUrl
	 * @return string
     */
    public function generateUrl()
	{
		$connectUrl = 'https://mp.weixin.qq.com/cgi-bin/componentloginpage?';

		$params = array(
			'component_appid' => $this->appId,
			'pre_auth_code' => $this->preAuthCode,
			'redirect_uri' => Url::getBaseUrl().'/mobileapps/wechat-third-party/setup',
		);

		$connectUrl .= http_build_query($params);

		return $connectUrl;
	}

	/**
	 * getAndSaveAuthorizationInfo
	 *
	 * @param $organizationId
	 * @param $authorizationCode
     */
    public function getAndSaveAuthorizationInfo($organizationId, $authorizationCode)
	{
		$params = array(
			'component_appid' => $this->appId,
			'authorization_code' => $authorizationCode,
		);
Logger::wechatThirdPartyLog('DEBUG getAndSaveAuthorizationInfo Access Token: '.$this->accessToken.PHP_EOL.'Params: '.serialize($params));

		$response = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/component/api_query_auth?component_access_token='.$this->accessToken, json_encode($params));
		Logger::wechatThirdPartyLog('Get Third-Party Authorization Info: '.$response);

		$response = json_decode($response);
		$response = $response->authorization_info;

		$params = array(
			'component_appid' => $this->appId,
			'authorizer_appid' => $response->authorizer_appid,
		);
		$response2 = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/component/api_get_authorizer_info?component_access_token='.$this->accessToken, json_encode($params));
		Logger::wechatThirdPartyLog('Get Authorizer Info: '.$response2);
		$response2 = json_decode($response2);
		$response2 = $response2->authorizer_info;

		//Save values in the database
		$sql = "INSERT INTO `WechatAuthorization` (
				`organizationId`,
				`appId`,
				`accessToken`,
				`expireDate`,
				`refreshToken`,
				`userName`
			) VALUES (
				:organizationId,
				:appId,
				:accessToken,
				:expireDate,
				:refreshToken,
				:userName
			)";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':organizationId', $organizationId, \PDO::PARAM_INT);
		$stmt->bindParam(':appId', $response->authorizer_appid, \PDO::PARAM_STR);
		$stmt->bindParam(':accessToken', $response->authorizer_access_token, \PDO::PARAM_STR);

		$expireDate = new \DateTime('now');
		$expireDate->add(\DateInterval::createFromDateString('+'.$response->expires_in.' second'));

		$stmt->bindValue(':expireDate', $expireDate->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
		$stmt->bindParam(':refreshToken', $response->authorizer_refresh_token, \PDO::PARAM_INT);
		$stmt->bindParam(':userName', $response2->user_name, \PDO::PARAM_INT);
		$stmt->execute();

		// Uncomment this code in case we want to update the info, but in general, once an unlink process happens, no record
		// should exist in that table
		// if ($this->db->rowCount() == 0)
		// {
		// 	$sql = "UPDATE `WechatAuthorization` SET `appId`=:appId, `accessToken`=:accessToken, `expireDate`=:expireDate, `refreshToken`=:refreshToken WHERE ";
		// 	$stmt = $this->db->prepare($sql);
		// 	$stmt->bindParam(':appId', $response->authorizer_appid, \PDO::PARAM_STR);
		// 	$stmt->bindParam(':accessToken', $response->authorizer_access_token, \PDO::PARAM_STR);
		// 	$stmt->bindValue(':expireDate', $expireDate->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
		// 	$stmt->bindParam(':refreshToken', $response->authorizer_refresh_token, \PDO::PARAM_INT);
		// 	$stmt->execute();
		// }
	}

	/**
	 * getAuthorizerAccessToken
	 *
	 * This will fetch the authorizer token for a given official account that is connected to our system
	 * and refresh the token if necessary
	 *
	 * @param $organizationId
	 *
	 * @return mixed
	 * @throws \Exception
     */
    public function getAuthorizerAccessToken($organizationId)
	{
		$sql = "SELECT 
				    `appId`, `accessToken`, `refreshToken`, `expireDate`
				FROM
				    `WechatAuthorization`
				WHERE
				    `organizationId` = :organizationId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':organizationId', $organizationId, \PDO::PARAM_INT);
		$stmt->execute();

		$obj = $stmt->fetch(\PDO::FETCH_OBJ);

		if ($obj === false)
			throw new \Exception(_('Could not get WeChat authorization info with the provided Organization ID'));

		$tokenExpireDate = new \DateTime($obj->expireDate);
		$now = new \DateTime('now');

		if ($tokenExpireDate < $now)
			return $obj->accessToken;

		//Refresh the token since it has expired
		$params = array(
			'component_appid' => $this->appId,
			'authorizer_appid' => $obj->appId,
			'authorizer_refresh_token' => $obj->refreshToken,
		);

		// Logger::wechatThirdPartyLog('DEBUG Refresh Token: AccessToken:'.$this->accessToken.PHP_EOL.'Params:'.json_encode($params));

		$response = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token='.$this->accessToken, json_encode($params));

		Logger::wechatThirdPartyLog('Refresh Authorizer Access Token: '.$response);

		$response = json_decode($response);

		$sql = "UPDATE 
					`WechatAuthorization` 
				SET 
					`accessToken`=:accessToken, 
					`expireDate`=:expireDate, 
					`refreshToken`=:refreshToken
				WHERE
					`organizationId`=:organizationId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':accessToken', $response->authorizer_access_token, \PDO::PARAM_STR);

		$expireDate = new \DateTime('now');
		$expireDate->add(\DateInterval::createFromDateString('+'.$response->expires_in.' second'));

		$stmt->bindValue(':expireDate', $expireDate->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
		$stmt->bindParam(':refreshToken', $response->authorizer_refresh_token, \PDO::PARAM_INT);
		$stmt->bindParam(':organizationId', $organizationId, \PDO::PARAM_INT);
		$stmt->execute();

		return $response->authorizer_access_token;
	}

	/**
	 * getAuthorizerAccessTokenByAuthorizerAppId
	 *
	 * @param $authorizerAppId
	 *
	 * @return mixed
	 * @throws \Exception
     */
    public function getAuthorizerAccessTokenByAuthorizerAppId($authorizerAppId)
	{
		$sql = "SELECT 
				    `appId`, `accessToken`, `refreshToken`, `expireDate`
				FROM
				    `WechatAuthorization`
				WHERE
				    `appId` = :appId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':appId', $authorizerAppId, \PDO::PARAM_STR);
		$stmt->execute();

		$obj = $stmt->fetch(\PDO::FETCH_OBJ);

		if ($obj === false)
			throw new \Exception(_('Could not get WeChat authorization info with the provided Organization ID'));

		$tokenExpireDate = new \DateTime($obj->expireDate);
		$now = new \DateTime('now');

		if ($tokenExpireDate < $now)
			return $obj->accessToken;

		//Refresh the token since it has expired
		$params = array(
			'component_appid' => $this->appId,
			'authorizer_appid' => $authorizerAppId,
			'authorizer_refresh_token' => $obj->refreshToken,
		);

		// Logger::wechatThirdPartyLog('DEBUG Refresh Token by App ID: AccessToken:'.$this->accessToken.PHP_EOL.'Params:'.json_encode($params));

		$response = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token='.$this->accessToken, json_encode($params));

		Logger::wechatThirdPartyLog('Refresh Authorizer Access Token: '.$response);

		$response = json_decode($response);

		if (isset($response->errcode))
		{
			if ($response->errcode == 61023) //refresh_token is invalid
				throw new \Exception('Your authorization tokens are invalid. You need to relink your Wechat account with our system');
			else
				throw new \Exception('There has been an unspecified error with your account\'s authorization token');
		}

		$sql = "UPDATE 
					`WechatAuthorization` 
				SET 
					`accessToken`=:accessToken, 
					`expireDate`=:expireDate, 
					`refreshToken`=:refreshToken
				WHERE
					`appId`=:appId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':accessToken', $response->authorizer_access_token, \PDO::PARAM_STR);

		$expireDate = new \DateTime('now');
		$expireDate->add(\DateInterval::createFromDateString('+'.$response->expires_in.' second'));

		$stmt->bindValue(':expireDate', $expireDate->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
		$stmt->bindParam(':refreshToken', $response->authorizer_refresh_token, \PDO::PARAM_INT);
		$stmt->bindParam(':appId', $authorizerAppId, \PDO::PARAM_STR);
		$stmt->execute();

		return $response->authorizer_access_token;
	}

	/**
	 * getAuthorizerUserNameByAppId
	 *
	 * @param $authorizerAppId
	 *
	 * @return mixed
	 * @throws \Exception
     */
    public function getAuthorizerUserNameByAppId($authorizerAppId)
	{
		$sql = "SELECT `userName` FROM `WechatAuthorization` WHERE `appId`=:appId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':appId', $authorizerAppId, \PDO::PARAM_STR);
		$stmt->execute();

		$obj = $stmt->fetch(\PDO::FETCH_OBJ);

		if ($obj === false)
			throw new \Exception(_('Could not get the authorizer user name with the provided app ID'));

		return $obj->userName;
	}

	/**
	 * getOrganizationIdByAuthorizerAppId
	 *
	 * @param $authorizerAppId
	 *
	 * @return mixed
	 * @throws \Exception
     */
    public function getOrganizationIdByAuthorizerAppId($authorizerAppId)
	{
		$sql = "SELECT `organizationId` FROM `WechatAuthorization` WHERE `appId`=:appId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':appId', $authorizerAppId, \PDO::PARAM_STR);
		$stmt->execute();

		$obj = $stmt->fetch(\PDO::FETCH_OBJ);

		if ($obj === false)
			throw new \Exception(_('Could not get the organization ID with the provided app ID'));

		return $obj->organizationId;
	}

	/**
	 * sendRichMediaMessageToUser
	 *
	 * @param $authorizerAppId
	 * @param $jsonMessage
	 *
	 * @throws \Exception
     */
    public function sendRichMediaMessageToUser($authorizerAppId, $jsonMessage)
	{
		//Find the authorizer access token
		$authorizerAccessToken = $this->getAuthorizerAccessTokenByAuthorizerAppId($authorizerAppId);

		// Logger::wechatThirdPartyLog('DEBUG sendRichMediaMessageToUser: '.json_encode($params).PHP_EOL.'Authorizer Access Token: '.$authorizerAccessToken);

		$response = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token='.$authorizerAccessToken, $jsonMessage);
		Logger::wechatThirdPartyLog('sendMessageToUser Response: '.$response);
	}

	/**
	 * sendMessageToUser
	 *
	 * @param $authorizerAppId
	 * @param $ToUserName
	 * @param $message
	 *
	 * @throws \Exception
     */
    public function sendMessageToUser($authorizerAppId, $ToUserName, $message, $messageType = 'text')
	{
		//Find the authorizer access token
		$authorizerAccessToken = $this->getAuthorizerAccessTokenByAuthorizerAppId($authorizerAppId);

		if ($messageType == 'text') {
			$params = array(
				'touser' => "$ToUserName",
				'msgtype' => $messageType,
				'text' => array(
					'content' => $message,
				),
			);
		} else if ($messageType == 'image') {
			$params = array(
				'touser' => "$ToUserName",
				'msgtype' => $messageType,
				'image' => array(
					'media_id' => $message,
				),
			);
		} else if ($messageType == 'voice') {
			$params = array(
				'touser' => "$ToUserName",
				'msgtype' => $messageType,
				'voice' => array(
					'media_id' => $message,
				),
			);
		} else if ($messageType == 'video') {
			$params = array(
				'touser' => "$ToUserName",
				'msgtype' => $messageType,
				'video' => array(
					'media_id' => $message,
				),
			);
		}

		// Logger::wechatThirdPartyLog('DEBUG sendMessageToUser: '.json_encode($params).PHP_EOL.'Authorizer Access Token: '.$authorizerAccessToken);

		$response = Rest::call('POST', 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token='.$authorizerAccessToken, json_encode($params, JSON_UNESCAPED_UNICODE));
		Logger::wechatThirdPartyLog('sendMessageToUser Response: '.$response);
	}

	/**
	 * getAuthorizerAppIdByLocationId
	 *
	 * @param $locationId
	 *
	 * @return bool
     */
    public function getAuthorizerAppIdByLocationId($locationId)
	{
		$sql = "SELECT 
				    wa.`appId`
				FROM
				    `locations` AS l
				JOIN
					`WechatAuthorization` AS wa ON wa.`organizationId`=l.`organization_id`
				WHERE
				    l.`location_id` = :locationId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':locationId', $locationId, \PDO::PARAM_INT);
		$stmt->execute();

		$obj = $stmt->fetch(\PDO::FETCH_OBJ);

		if ($obj !== false)
			return $obj->appId;
		else
			return false;
	}

	/**
	 * getUserOpenIdByUserId
	 *
	 * @param $userId
	 *
	 * @return bool
     */
    public function getUserOpenIdByUserId($userId)
	{
		$sql = "SELECT `openId` FROM `UserWechat` WHERE `userId`=:userId";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':userId', $userId, \PDO::PARAM_INT);
		$stmt->execute();

		$obj = $stmt->fetch(\PDO::FETCH_OBJ);

		if ($obj !== false)
			return $obj->openId;
		else
			return false;
	}

	/**
	 * getUserProfile
	 *
	 * @param $openId
	 * @param $organizationId
	 * @param $lang
	 *
	 * @return WechatUserEntity
	 * @throws \Exception
     */
    public function getUserProfile($openId, $organizationId, $lang)
	{
		$accessToken = $this->getAuthorizerAccessToken($organizationId);

		if ($lang === UserModel::LANG_ZH)
			$lang = 'zh_CN';
		else
			$lang = 'en';

		$params = array(
			'access_token' => $accessToken,
			'openid' => $openId,
			'lang' => $lang,
		);

		// Logger::wechatThirdPartyLog('DEBUG getUserProfile openId: '.$openId.PHP_EOL.'organizationId: '.$organizationId.PHP_EOL.'lang: '.$lang);

		$profile = json_decode(Rest::call('GET', 'https://api.wechat.com/cgi-bin/user/info', $params));

		Logger::wechatThirdPartyLog('Response from wechat user/info: '.serialize($profile));

		if ($profile === false)
			throw new \Exception('Could not get the user profile');

		$user = new WechatUserEntity();
		$user->openId = isset($profile->openid) ? $profile->openid : '';
		$user->userName = isset($profile->nickname) ? $profile->nickname : '';
		$user->gender = isset($profile->sex) ? $profile->sex : '';
		$user->language = isset($profile->language) ? $profile->language : '';
		$user->city = isset($profile->city) ? $profile->city : ''; 
		$user->province = isset($profile->province) ? $profile->province : '';
		$user->country = isset($profile->country) ? $profile->country : '';
		$user->avatarUrl = isset($profile->headimgurl) ? $profile->headimgurl : '';

		return $user;
	}

	//This is similar to getUserProfile but throws an exception not only on the call failing but also if all the
	//values for that return are null except the openId. That is because the only way to get that situation is when a user is not
	//a subscriber/follower for that account. In the end, this call is mainly used to check that Wechat can properly return
	//user details for this openID, so that we don't create an invalid user
	public function getUserProfileEx($openId, $organizationId, $lang = UserModel::LANG_EN)
	{
		$accessToken = $this->getAuthorizerAccessToken($organizationId);

		if ($lang === UserModel::LANG_ZH)
			$lang = 'zh_CN';
		else
			$lang = 'en';

		$params = array(
			'access_token' => $accessToken,
			'openid' => $openId,
			'lang' => $lang,
		);

		// Logger::wechatThirdPartyLog('DEBUG getUserProfile openId: '.$openId.PHP_EOL.'organizationId: '.$organizationId.PHP_EOL.'lang: '.$lang);

		$profile = json_decode(Rest::call('GET', 'https://api.wechat.com/cgi-bin/user/info', $params));

		Logger::wechatThirdPartyLog('Response from wechat user/info: '.serialize($profile));

		if ($profile === false)
			throw new \Exception('Could not get the user profile');

		$user = new WechatUserEntity();
		$user->openId = isset($profile->openid) ? $profile->openid : null;
		$user->userName = isset($profile->nickname) ? $profile->nickname : null;
		$user->gender = isset($profile->sex) ? $profile->sex : null;
		$user->language = isset($profile->language) ? $profile->language : null;
		$user->city = isset($profile->city) ? $profile->city : null; 
		$user->province = isset($profile->province) ? $profile->province : null;
		$user->country = isset($profile->country) ? $profile->country : null;
		$user->avatarUrl = isset($profile->headimgurl) ? $profile->headimgurl : null;

		if ($user->language === null)
			throw new \Exception('An empty profile has been returned');

		return $user;
	}

	/**
	 * processSubscribeEvent
	 *
	 * @param $event
	 */
	public function processSubscribeEvent($event){
		Logger::debugLog('processSubscribeEvent Event fromUserName: '. $event->fromUserName);
		//initialize the models
		$wechatModel = new WechatModel();
		$locationModel = new LocationModel();

		//get the location via two different ways.
		//1. LocationDetail (WifiConnect Event)
		//2. WifiConnect (Subscribe Event)
		$location = null;
		$locationByShopID = null;
		//If the shopId in the event is set
		if(isset($event->shopId)){
			//Get the location by the shopdId
			$locationByShopID = $locationModel->getLocationByWechatShopId($event->shopId);

			//If there is an result and the locationID is set
			if($locationByShopID && isset($locationByShopID->locationId)){
				//Use the locationID from the shopId
				$location = $locationByShopID->locationId;
			}
		}

		//If there isn't a location already
		if(!$location){
			//Check if the fromUserName (openID) is set
			if(isset($event->fromUserName)){
				//get the location by the openId
				$locationByOpenID = $wechatModel->getLocationByWifiConnectOpenId($event->fromUserName);

				//If there is an location
				if($locationByOpenID && isset($locationByOpenID->locationId)){
					//Use the locationId from the openId
					$location = $locationByOpenID->locationId;
				}
			}
		}

		//Track if a user subscribed after a Wechat Wifi Connect event
		$userId = $wechatModel->checkWifiConnectUser($event->fromUserName);

		//Check if the userid is set
		if ($userId !== false){
			//Check if the locationID is set
			if (isset($location)) {
				//set the WechatWifiSubscriber
				$wechatModel->setUserWechatWifiSubscriber($event->fromUserName, $location);
			}
		}
	}

	/**
	 * deleteUser
	 * @param $openId
	 */
	public function deleteUser($openId)
	{
		$wechatModel = new WechatModel();
		$user = $wechatModel->getUserByOpenId($openId);

		if ($user === false)
		{
			Logger::wechatThirdPartyLog('Unsubscribe Delete User: Could not find user with open ID '.$openId);
			return;
		}

		Logger::wechatThirdPartyLog('Unsubscribe Deleting user ID: '.$user->userId);

		try{

			$sql = "DELETE FROM `Message` WHERE `userId`=:userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $user->userId, \PDO::PARAM_INT);
			$stmt->execute();

			$sql = "DELETE FROM `UserDetail` WHERE `userId`=:userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $user->userId, \PDO::PARAM_INT);
			$stmt->execute();

			$sql = "DELETE FROM `UserThread` WHERE `userId`=:userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $user->userId, \PDO::PARAM_INT);
			$stmt->execute();

			$sql = "DELETE FROM `UserWechat` WHERE `userId`=:userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $user->userId, \PDO::PARAM_INT);
			$stmt->execute();

			$sql = "DELETE FROM `WechatWifiSubscriber` WHERE `userId`=:userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $user->userId, \PDO::PARAM_INT);
			$stmt->execute();

			$sql = "DELETE FROM `UserRegistration` WHERE `userId`=:userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $user->userId, \PDO::PARAM_INT);
			$stmt->execute();

			$sql = "DELETE FROM `users` WHERE `user_id`=:userId";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':userId', $user->userId, \PDO::PARAM_INT);
			$stmt->execute();

			Logger::wechatThirdPartyLog('Unsubscribe Deleted user ID: '.$user->userId.' successfully');
		}
		catch (\Exception $e)
		{
			Logger::wechatThirdPartyLog('Unsubscribe Error while deleting user ID: '.$user->userId.'. message: '.$e->getMessage());	
		}
	}

	public function unlinkWechat($organizationId)
	{
		try
		{
			$sql = "DELETE FROM `WechatAuthorization` WHERE `organizationId`=:organizationId LIMIT 1";
			$stmt = $this->db->prepare($sql);
			$stmt->bindParam(':organizationId', $organizationId, \PDO::PARAM_INT);
			$stmt->execute();

			Logger::wechatThirdPartyLog("Unlinked $organizationId from WeChat third party services successfully");
		}
		catch (\Exception $e)
		{
			Logger::wechatThirdPartyLog('Unlink $organizationId error: '.$e->getTraceAsString());	
		}
	}

	private function is_json($string) 
	{
 		json_decode($string);
	 	return (json_last_error() == JSON_ERROR_NONE);
	}
}
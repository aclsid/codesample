<?php

namespace H5Messaging\Controller;

use Application\Controller\BaseController;

use Zend\View\Model\ViewModel;
use Zend\Session\Container;

use Application\Model\AccountKitModel;
use Application\Model\CampaignModel;
use Application\Model\OrganizationModel;
use Application\Model\LocationModel;
use Application\Model\WechatAuthorizationModel;
use Application\Model\OrganizationMenuModel;
use Marketingcenter\Model\TokenModel;

use Application\Utils\MemoryCache;
use Application\Utils\Url;
use Application\Utils\Csrf;
use Application\Utils\Rest;
use Application\Utils\Logger;

use Application\Entity\RequestOptionsEntity;

class H5MessagingController extends BaseController
{
	private $environment;
	public function __construct()
	{
		// Do not call the parent constructor here
		// otherwise we will get redirected to the login screen
		// but do log in with a guest user if needed
		// parent::__construct();

		$this->environment = MemoryCache::get('environment');

		if ($this->environment === false || $this->environment === null)
		{
			$this->environment = json_decode(file_get_contents(ROOT_PATH.'/config/environment.json'));
			MemoryCache::set('environment', serialize($this->environment), 3600);
		}
		else
			$this->environment = unserialize($this->environment);
	}

	public function indexAction()
	{
		$campaignModel = new CampaignModel();
		$organizationModel = new OrganizationModel();
		$locationModel = new LocationModel();
		$wechatAuthorizationModel = new WechatAuthorizationModel();
		$organizationMenuModel = new OrganizationMenuModel();

		$locationId = (int)$this->params()->fromRoute('id');
		$storyId = $this->getEx('storyId', new RequestOptionsEntity(array(
			'dataType' => 'int',
			'defaultValue' => 0,
			)));

		$organization = $organizationModel->getOrganizationByLocationId($locationId);

		$campaignDetails = $campaignModel->getLatestCampaignByOrganizationId($organization->organizationId);
		$locationDetails = $locationModel->getLocationByIdEx($locationId);

		$wechatCredentials = $wechatAuthorizationModel->getByOrganizationId($organization->organizationId);

		$organizationMenuItems = $organizationMenuModel->getFullOrganizationMenuByOrganizationId($organization->organizationId);

		$loginRedirectUrl = null;

		if ($this->environment->serverLocation == 'US')
		{
			$loginRedirectUrl = "http://locationmagnet.com/h5messaging/account-kit-login";
		}
		else
		{
			if ($wechatCredentials !== false)
			{
				$loginRedirectUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?';
				$loginRedirectUrl .= http_build_query(array(
					'appid' => $wechatCredentials->appId,
					'redirect_uri' => Url::getBaseUrl().'/h5authex',
					'response_type' => 'code',
					'scope' => 'snsapi_userinfo',
					'state' => $locationId,
					));
				$loginRedirectUrl .= '#wechat_redirect';            
			}			
		}

		$view = new ViewModel(array(
			'campaignDetails' => $campaignDetails,
			'locationId' => $locationId,
			'locationDetails' => $locationDetails,
			'organization' => $organization,
			'apiServerUrl' => $this->environment->apiServerUrl,
			'storyId' => $storyId,
			'baseUrl' => Url::getBaseUrl(),
			'loginRedirectUrl' => $loginRedirectUrl,
			'organizationMenuItems' => $organizationMenuItems,
			));
		$view->setTerminal(true);
		return $view;
	}

	public function accountKitLoginAction()
	{
		$view = new ViewModel(array(
			'csrfToken' => Csrf::generateToken(),
			'facebookAppId' => $this->environment->facebook->appId,
			'apiVersion' => $this->environment->facebook->version,
		));
		$view->setTerminal(true);
		return $view;
	}

	public function accountKitProcessLoginAction()
	{
		$accountKitModel = new AccountKitModel();
		$tokenModel = new TokenModel();

		try
		{
			// Logger::debugLog('Account Kit Process Login POST: '.serialize($_POST));

			$csrfToken = $this->postEx('csrf_nonce', new RequestOptionsEntity(array(
				'dataType' => 'string',
			)));

			$code = $this->postEx('code', new RequestOptionsEntity(array(
				'dataType' => 'string',
			)));

			// if (Csrf::validateToken($csrfToken) === false)
			// 	throw new \Exception('Invalid CSRF token');

			$appAccessToken = implode('|', array(
				'AA', 
				$this->environment->facebook->appId,
				$this->environment->facebook->accountKitAppSecret
			));
			// Logger::debugLog('App Access Token: '.serialize($appAccessToken));

			$params = array(
				'grant_type' => 'authorization_code',
				'code' => $code,
				'access_token' => $appAccessToken,
			);
			// Logger::debugLog('Code: '.serialize($code));

			$meBaseUrl = 'https://graph.accountkit.com/'.$this->environment->facebook->version.'/me';
			$tokenExchangeBaseUrl = 'https://graph.accountkit.com/'.$this->environment->facebook->version.'/access_token';

			$tokenExchangeResponse = json_decode(Rest::call('GET', $tokenExchangeBaseUrl, $params));
			// Logger::debugLog('Token Exchange Response: '.serialize($tokenExchangeResponse));

			$params = array(
				'access_token' => $tokenExchangeResponse->access_token,
			);
			$meResponse = Rest::call('GET', $meBaseUrl, $params);
			// Logger::debugLog('Me Response: '.serialize($meResponse));


			$userId = $accountKitModel->checkAccountExists($tokenExchangeResponse->id);

			if ($userId === false)
				$userId = $accountKitModel->linkAccount($tokenExchangeResponse->id, $tokenExchangeResponse->access_token);

			$tokenModel->linkToken($userId, $tokenExchangeResponse->access_token);

			$view = new ViewModel(array(
				'userAccessToken' => $tokenExchangeResponse->access_token,
				// 'expiresAt' => $tokenExchangeResponse->expires_at,
				'accountKitId' => $tokenExchangeResponse->id,
				'userId' => $userId,
				// 'phoneNum' => $tokenExchangeResponse->phone,
				// 'emailAddr' => $tokenExchangeResponse->email,
			));
			$view->setTerminal(true);
			return $view;			
		}
		catch(\Exception $e)
		{
			$view = new ViewModel(array(
				'errorMessage' => $e->getMessage(),
			));
			$view->setTerminal(true);
			return $view;						
		}
	}
}

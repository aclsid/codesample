declare let options:any;
declare let $:any;
declare let moment:any;

import {Chart} from '../utils/chart';

(() => {
	if (options.recentActivity.length === 0)
	{
		$('#recent_activity').find('.message').toggleClass('hidden');	
		return;
	}

	let chart = new Chart(options.recentActivity, options.chartSelectorId);
	chart.plot();
})();

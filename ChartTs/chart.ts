declare let $:any;
declare let moment:any;

export class Chart 
{
	private selectorId:number;
	private dataSet:[any];
	private lineColor:string;

	constructor(dataSet:[Object], selectorId:number, lineColor:string = '#C7754C') {
		this.dataSet = this.convertFromJSONDataSet(dataSet);
		this.selectorId = selectorId;
		this.lineColor = lineColor;
	}

	private convertFromJSONDataSet(dataSet:any)
	{
		let newDataSet:any = new Array();
		
		for (let item of dataSet)
		{
			//We add an initial element to make the chart look good when the dataset only has a few items
			if (newDataSet.length === 0)
				newDataSet.push([moment(item.date).subtract(1, 'days').valueOf(),0]);

			item.date = moment(item.date).valueOf();
			newDataSet.push([item.date, parseInt(item.balance)]);
		}

		return newDataSet;
	}

	plot() {
		let plot1 = $.jqplot(this.selectorId, [this.dataSet], {
			seriesColors:[this.lineColor],
			axes:{
				xaxis:{
					renderer:$.jqplot.DateAxisRenderer
				}
			},
			series:[{lineWidth:4, markerOptions:{style:'square'}}]
		});
	}
}
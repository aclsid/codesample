<?php
namespace Migration\Utils;

//Modified from http://codepad.org/thpzCOyn

class TopologicalSort
{
	static public function sort($nodeids, $edges)
	{
		$L = $S = $nodes = array();

		foreach($nodeids as $id)
		{
			$nodes[$id] = array('in'=>array(), 'out'=>array());

			foreach($edges as $e)
			{
				if ($id==$e[0])
					$nodes[$id]['out'][]=$e[1];

				if ($id==$e[1])
					$nodes[$id]['in'][]=$e[0];
			}
		}
		foreach ($nodes as $id=>$n)
		{
			if (empty($n['in']))
				$S[]=$id;
		}
		while (!empty($S))
		{
			$L[] = $id = array_shift($S);
			foreach($nodes[$id]['out'] as $m)
			{
				$nodes[$m]['in'] = array_diff($nodes[$m]['in'], array($id));

				if (empty($nodes[$m]['in']))
					$S[] = $m;
			}
			$nodes[$id]['out'] = array();
		}
		foreach($nodes as $n)
		{
			if (!empty($n['in']) or !empty($n['out']))
				throw new \Exception("Circular dependency detected."); // not sortable as graph is cyclic
		}
		return array_reverse($L);
	}
}
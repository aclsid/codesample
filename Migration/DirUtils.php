<?php
namespace Migration\Utils;

class DirUtils
{
	static public function getFiles($path, $regexFilter = null)
	{
		$directory = new \RecursiveDirectoryIterator($path);

		$filter = new DirFilter($directory);

		if ($regexFilter !== null)
			$filter = new FilenameFilter($filter, $regexFilter);

		return $filter;
	}
}

abstract class FilesystemRegexFilter extends \RecursiveRegexIterator
{
	protected $regex;
	public function __construct(\RecursiveIterator $it, $regex)
	{
		$this->regex = $regex;
		parent::__construct($it, $regex);
	}
}

class FilenameFilter extends FilesystemRegexFilter
{
	// Filter files against the regex
	public function accept()
	{
		return ( !$this->isFile() || preg_match($this->regex, $this->getFilename()));
	}
}

class DirFilter extends \RecursiveFilterIterator
{
	// Filter directories
	public function accept()
	{
		return ( !$this->isDir());
	}
}
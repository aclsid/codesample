declare let promise:any;
declare let window:any;

export class Ajx {

	private ajx:XMLHttpRequest;

	constructor() {
		if (window.XMLHttpRequest)
			this.ajx = new XMLHttpRequest();
		else if (window.ActiveXObject)
		{
			this.ajx = new ActiveXObject("Msxml2.XMLHTTP");
			if (this.ajx === undefined)
				this.ajx = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	get(url: string):Object {
		return this.request('GET', url);
	}

	post(url: string):Object {
		return this.request('POST', url);
	}

	private request(method: string, url: string) {
		
		let p = new promise.Promise();
		let self = this;


		this.ajx.open(method, url);
		this.ajx.setRequestHeader('Content-Type', 'application/json');

		this.ajx.onload = function(event:Event) {
			if (self.ajx.status === 200)
				p.done(null, 'Ok');
			else
				p.fail(null, 'Failed');

		};

		return p;
	}
}